TRUNCATE TABLE user_info;
TRUNCATE TABLE character_class;
TRUNCATE TABLE rpgcharacter;
TRUNCATE TABLE class_abilities;

INSERT INTO user_info (user_name, user_password)
VALUES
('Mathias', 10111),
('Alexander', 10111),
('Dean Von', 10111),
('DewaltLadders', 10111),
('xXn00bPWNrXx', 10111);

INSERT INTO character_class (character_class) VALUES
 ('Wizard'), ('Necromancer'), ('Sorcerer'), ('Bard'), ('Paladin'),
 ('Druid'), ('Rouge'), ('Cleric');

INSERT INTO rpgcharacter (character_name, character_class_id, character_level, user_id)
VALUES
('Dean Von Destroyer', 1, 10, 3), ('Pub Dean Von', 4, 5, 3),
('Dewalt Ladder Tosser', 6, 8, 4), ('Pub Dewaltion', 8, 3, 4), ('N00bSlayer', 7, 20, 4),
('J0gge', 3, 10, 1), ('PubJ0ggster', 5, 5, 1), ('Edge-Sama', 7, 70, 5),
('I hate my mom', 7, 1, 2);


INSERT INTO class_abilities (ability_name, ability_base_damage, character_class_id, level_requirement)
VALUES
('Fire Ball', 5, 1, 1),
('Blizzard', 10, 1, 5),
('Summon Flame Imp', 0, 2, 1),
('Raise Dead', 0, 2, 5),
('Ice Lance', 5, 3, 1),
('Summon Flame demon', 0, 3, 10),
('Song of the warrior', 0, 4, 1),
('Devine Hymn', -50, 4, 5),
('The Hymn Neptun of the waves', 150, 4, 20),
('Divine hammer', 20, 5, 1),
('Prayers of the Gods', 0, 5, 4),
('Holy Slash', 70, 5, 10),
('Natures Spirit', 0, 6, 1),
('Leaves of the Forest', 30, 6, 5),
('One with Nature, Beast transformation', 0, 6, 15),
('Shadow Dash', 10, 7, 1),
('Backstab', 60, 7, 5),
('Silent Assassin', 0, 7, 10),
('Words of the Holy People', 10, 8, 1),
('Holy Hammer', 100, 8, 10),
('Mass Heal', -100, 8, 30);