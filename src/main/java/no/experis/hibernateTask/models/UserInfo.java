package no.experis.hibernateTask.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("user_id")
    @Column(name = "user_id")
    public int userID;

    @NotNull
    @JsonProperty("user_name")
    @Column(name = "user_name", unique = true)
    public String userName;

    @NotNull
    @JsonProperty("user_password")
    @Column(name = "user_password")
    public String userPassword;

    @OneToMany(cascade = CascadeType.ALL,
    orphanRemoval = true)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ElementCollection(targetClass=RPGCharacter.class)
    @Size(min=0, max=3)
    public List<RPGCharacter> characters = new ArrayList<RPGCharacter>();
}
