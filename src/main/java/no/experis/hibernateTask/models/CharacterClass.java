package no.experis.hibernateTask.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class CharacterClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_class_id")
    public int classID;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "character_class_id", referencedColumnName = "character_class_id")
    public List<RPGCharacter> character = new ArrayList<RPGCharacter>();

    @NotNull
    @Column(name = "character_class")
    public String characterClass;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "character_class_id", referencedColumnName = "character_class_id")
    @ElementCollection(targetClass = ClassAbilities.class)
    public List<ClassAbilities> classAbilities = new ArrayList<ClassAbilities>();

}
