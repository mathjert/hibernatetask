package no.experis.hibernateTask.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class RPGCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    public int characterID;

    @NotNull
    @Column(name = "character_name", unique = true)
    public String characterName;

    @NotNull
    @Column(name = "character_level")
    public int level;

    @Column(name = "user_id")
    @NotNull
    public int userID;

    @Column(name = "character_class_id")
    @NotNull
    public int classID;
}
