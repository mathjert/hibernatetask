package no.experis.hibernateTask.models;

public enum EnumCharacterClass {
    MAGE("Mage"),
    NECROMANCER("Necromancer"),
    PALADIN("Paladin"),
    DRUID("Druid"),
    SORCERER("Sorcerer"),
    ROUGE("Rouge"),
    BARD("Bard"),
    ASSASSIN("Assassin"),
    PRIEST("Priest"),
    CLERIC("Cleric"),
    WARRIOR("Warrior");

    private String characterClass;
    private EnumCharacterClass(String characterClass){
        this.characterClass = characterClass;
    }

    public String getCharacterClass(){
        return this.characterClass;
    }

}
