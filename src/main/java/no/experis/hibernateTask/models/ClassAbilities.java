package no.experis.hibernateTask.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class ClassAbilities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ability_id")
    public int abilityID;

    @Column(name = "ability_name")
    public String abilityName;

    @Column(name = "ability_base_damage")
    public int baseDamage;

    @Column(name = "level_requirement")
    @NotNull
    public int levelRequirement;

    @Column(name = "character_class_id")
    public int classID;


}
