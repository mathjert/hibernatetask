package no.experis.hibernateTask.repository;
import no.experis.hibernateTask.models.ClassAbilities;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ClassAbilitiesRepository extends JpaRepository<ClassAbilities, Integer>{

    public ClassAbilities getClassAbilitiesByAbilityID(int id);

}
