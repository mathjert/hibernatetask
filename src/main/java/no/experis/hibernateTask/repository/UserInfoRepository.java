package no.experis.hibernateTask.repository;

import no.experis.hibernateTask.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserInfoRepository extends JpaRepository<UserInfo, Integer>{

    UserInfo findUserInfoByUserID(int id);

}
