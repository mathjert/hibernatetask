package no.experis.hibernateTask.repository;

import no.experis.hibernateTask.models.CharacterClass;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CharacterClassRepository extends JpaRepository<CharacterClass, Integer>{

    CharacterClass findCharacterClassByClassID(int id);

}