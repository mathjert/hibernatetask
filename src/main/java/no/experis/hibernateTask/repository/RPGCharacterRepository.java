package no.experis.hibernateTask.repository;
import no.experis.hibernateTask.models.RPGCharacter;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RPGCharacterRepository extends JpaRepository<RPGCharacter, Integer>{

    RPGCharacter findRPGCharacterByCharacterID(int id);

}