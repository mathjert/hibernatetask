package no.experis.hibernateTask.controllers;

import no.experis.hibernateTask.models.CharacterClass;
import no.experis.hibernateTask.repository.CharacterClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CharacterClassController {

    @Autowired
    private final CharacterClassRepository CCRepository;

    public CharacterClassController(CharacterClassRepository CCRepository){
        this.CCRepository = CCRepository;
    }

    @GetMapping("/classes")
    public List<CharacterClass> getAllClasses(){
        return CCRepository.findAll();
    }

    @GetMapping("/class/{id}")
    public CharacterClass getClassById(@PathVariable(value = "id") int id){
        return CCRepository.findCharacterClassByClassID(id);
    }

    @GetMapping("/class/{className}")
    public CharacterClass getClassByClassName(@PathVariable(value = "className") String className){
        for(CharacterClass c : CCRepository.findAll()){
            if(c.characterClass.equals(className))
                return c;
        }
        return null;
    }
}
