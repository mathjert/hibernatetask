package no.experis.hibernateTask.controllers;

import no.experis.hibernateTask.models.RPGCharacter;
import no.experis.hibernateTask.repository.RPGCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RPGCharacterController {

    @Autowired
    private final RPGCharacterRepository RPGCRepository;

    @Autowired

    public RPGCharacterController(RPGCharacterRepository RPGCRepository){
        this.RPGCRepository = RPGCRepository;
    }

    @GetMapping("/characters")
    public List<RPGCharacter> getAllCharacters(){
        return RPGCRepository.findAll();
    }

    @GetMapping("/character/{id}")
    public RPGCharacter findCharacterById(@PathVariable(value = "id") int id){
        return RPGCRepository.findRPGCharacterByCharacterID(id);
    }

    @PostMapping("/character")
    public RPGCharacter postCharacter(@RequestBody RPGCharacter rpgc){
        try{
            // sjekk om bruker finnes.
            return RPGCRepository.save(rpgc);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

}
