package no.experis.hibernateTask.controllers;

import no.experis.hibernateTask.models.CharacterClass;
import no.experis.hibernateTask.models.ClassAbilities;
import no.experis.hibernateTask.models.RPGCharacter;
import no.experis.hibernateTask.repository.UserInfoRepository;

import no.experis.hibernateTask.models.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

// tmp imports for population
import no.experis.hibernateTask.repository.CharacterClassRepository;



import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserInfoController {

    @Autowired
    private final UserInfoRepository UIRepository;
    private final CharacterClassRepository CCRepository;

    public UserInfoController(UserInfoRepository UIRepository, CharacterClassRepository CCRepository) {
        this.UIRepository = UIRepository;
        this.CCRepository = CCRepository;
    }

    @GetMapping("/users")
    public List<String> users(){
        return UIRepository.findAll().stream().map(e -> e.userName).collect(Collectors.toList());
    }

    @GetMapping("/user/{id}")
    public UserInfo findUserById(@PathVariable(value="id") int id){
        return UIRepository.findUserInfoByUserID(id);
    }

    @PostMapping("/user")
    public UserInfo addUser(@RequestBody UserInfo ui){
        try{
            if(ui.userID != 0){
                UserInfo uiTest = UIRepository.findUserInfoByUserID(ui.userID);
                if(!uiTest.userName.equals(ui.userName) || !uiTest.userPassword.equals(ui.userPassword))
                    return null;
            }
            // sjekke character?
            return UIRepository.save(ui);
        }catch (Exception e){
            //e.printStackTrace();
            System.err.println(e.getMessage());
            return null;
        }

    }

    @GetMapping("/populate/database")
    public List<UserInfo> populateDatabase(){
        try{
            UserInfo u1 = new UserInfo();
            u1.userName = "Dean & Dewalt Couple Account";
            u1.userPassword = "DeanWalt";

            RPGCharacter rpg1 = new RPGCharacter();
            rpg1.characterName = "Dean Von The Destroyer";
            rpg1.level = 1;

            CharacterClass cc1 = new CharacterClass();
            cc1.characterClass = "n00b PWNr";

            ClassAbilities ca1 = new ClassAbilities();
            ca1.abilityName = "Sleep with opponents mom";
            ca1.baseDamage = 5;
            ca1.levelRequirement = 1;

            u1.characters.add(rpg1);

            cc1.classAbilities.add(ca1);
            cc1.character.add(rpg1);

            //UIRepository.save(u1);
            CCRepository.save(cc1);

            RPGCharacter rpg2 = new RPGCharacter();
            rpg2.characterName = "Dewalt LigtningFingers";
            rpg2.level = 1;

            CharacterClass cc2 = new CharacterClass();
            cc2.characterClass = "Champignon of Typos";

            ClassAbilities ca2 = new ClassAbilities();
            ca2.levelRequirement = 1;
            ca2.abilityName = "Spore cloud of keyboard confusion";
            ca2.baseDamage = 0;
            cc2.classAbilities.add(ca2);
            cc2.character.add(rpg2);

            u1.characters.add(rpg2);

            CCRepository.save(cc2);
            UIRepository.save(u1);


            return UIRepository.findAll();

        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        }

    }
}
