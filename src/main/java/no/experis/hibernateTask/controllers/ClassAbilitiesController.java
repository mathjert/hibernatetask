package no.experis.hibernateTask.controllers;

import no.experis.hibernateTask.models.ClassAbilities;
import no.experis.hibernateTask.repository.ClassAbilitiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClassAbilitiesController {

    @Autowired
    private final ClassAbilitiesRepository CARepository;

    public ClassAbilitiesController(ClassAbilitiesRepository CARepository){
        this.CARepository = CARepository;
    }

    @GetMapping("/classabilities")
    public List<ClassAbilities> getAllClassAbilities(){
        return CARepository.findAll();
    }
}
