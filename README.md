# Hibernate task 
Hibernate task for the final week of the java course.  
This task sets up a game character for a given user.

### Regrets
Some error handling before delivering as the case was published, forcing me to work on that.

### General usage.
Would propose to use a get for both /users and /classes to have a look
at the general setup of the JSON objects.
When posting, exclude the id-s, and pass the new object.

### General notes
I truncate all tables on startup and fill in initial values. Learned that truncate for 
MySQL drops the tables before creating them again. This works fairly ok, so that the ID-s of 
the different rows start from 1 on every startup. 